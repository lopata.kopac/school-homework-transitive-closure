#pragma once
#include <vector> 
#include <iostream>
#include <algorithm>
#include <sstream>

using namespace std;
/*! \file Relationship.h
	\brief Untilites for working with Graph

	Details.
*/

/*! \var struct Item
	\brief Struct for  grarf elements.

	Details.
*/
struct Item {
	vector<int> childs;
};

//!  Relationship class. 
/*!
  Class for working with graph.
*/

class Relationship
{
private:
	vector<Item> items;
	int count;

	//! A create TransitiveClosure graf for first element.
	/*!
	  \param index of root.
	  \param result created graph.
	*/
	void transitiveClosure(int index, Relationship& result);

	//! A create TransitiveClosure graf for odhers elements.
	/*!
	  \param index of root.
	  \param index of child.
	  \param result created graph.
	*/
	void transitiveClosure(int mainIndex, int index, Relationship& result);
public:
	//! A constructor.
	/*!
	  A more elaborate description of the constructor.
	*/
	Relationship(int count);

	//! A destructor.
	/*!
	  A more elaborate description of the destructor.
	*/
	~Relationship();

	//! A add child to graph.
	/*!
	  \param itemIndex rood index.
	  \param value child index.
	*/
	void addChild(int itemIndex, int value);

	//! A short chin on index.
	/*!
	  \param index if rood element to sort.
	*/
	void shortChilds(int index);

	//! A getting TransitiveClosure graf.
	/*!
	  \return create new graph.
	*/
	Relationship getTransitiveClosure();

	//! A gett root on index.
	/*!
	  \param index od rood.
	  \return root on index.
	*/
	Item getItem(int index);

	//! A gett all roots.
	/*!
	  \return all roots.
	*/
	vector<Item> getItems();

	//! A gett parshed graph.
	/*!
	  \return parshed graph to string.
	*/
	string toString();
};

