#pragma once
#include "pch.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Relationship.h"


using namespace std;

/*! \file Until.h
	\brief Untilites for loading and sawing Graph

	Details.
*/

/*! \fn Relationship readRelationshipFromFile(string filename)
	\brief Load and build graph.
	\param filename name of Imput File.
*/
Relationship readRelationshipFromFile(string filename);

/*! \fn void saveRelationship(string filename, Relationship content)
	\brief Write graph to file.
	\param filename Name of ouput file.
	\param content Graf zou want save.
*/
void saveRelationship(string filename, Relationship content);