#include "pch.h"
#include "Until.h"

Relationship readRelationshipFromFile(string filename) 
{
	string line;
	ifstream myfile(filename);
	std::unique_ptr<Relationship> clousure;

	if (myfile.is_open())
	{
		getline(myfile, line);
		int count = stoi(line);
		unique_ptr<Relationship> tempClousure(new Relationship(count));
		clousure = move(tempClousure);

		while (getline(myfile, line))
		{
			int a, b;

			stringstream ss;
			ss << line;

			ss >> a;
			ss >> b;

			clousure->addChild(a, b);
		}

		myfile.close();
	}

	else 
	{
		cout << "ResourceHolder::load - Failed to load " << filename << endl;
		throw std::runtime_error("ResourceHolder::load - Failed to load " + filename);
	}

	return *clousure;
}

void saveRelationship(string filename, Relationship content) 
{
	ofstream myfile;
	myfile.open(filename);
	myfile << content.toString();
	myfile.close();
}