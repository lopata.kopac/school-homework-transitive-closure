#include "pch.h"
#include "Relationship.h"

Relationship::Relationship(int count = 0)
{
	this->count = count;

	items.resize(count);

	for (size_t i = 0; i < items.size(); i++) 
	{
		items[i].childs = {};
	}
}

Relationship::~Relationship()
{
}

void Relationship::addChild(int itemIndex, int value)
{
	items[itemIndex].childs.push_back(value);
}

void Relationship::shortChilds(int index) 
{
	sort(items[index].childs.begin(), items[index].childs.end());
}

Relationship Relationship::getTransitiveClosure()
{
	auto result = make_shared<Relationship>(count);
	for (size_t i = 0; i < count; i++) {
		transitiveClosure(i, *result);
	}

	return *result;
}

void Relationship::transitiveClosure(int index, Relationship& result)
{
	vector<int> tempChilds = items[index].childs;
	for (size_t i = 0; i < tempChilds.size(); i++) 
	{
		transitiveClosure(index, tempChilds[i], result);
	}
	
	result.shortChilds(index);
}

void Relationship::transitiveClosure(int mainIndex, int index, Relationship& result)
{
	Item item = result.getItem(mainIndex);
	for (size_t i = 0; i < item.childs.size(); i++)
	{
		if (item.childs[i] == index)
			return;
	}

	result.addChild(mainIndex, index);

	vector<int> tempChilds = items[index].childs;
	for (size_t i = 0; i < tempChilds.size(); i++) 
	{
		transitiveClosure(mainIndex, tempChilds[i], result);
	}
}

Item Relationship::getItem(int index)
{
	if(index < items.size())
		return items[index];

	return items[0];
}

vector<Item> Relationship::getItems()
{
	return items;
}

string Relationship::toString()
{
	stringstream ss;
	ss << count << endl;
	for (size_t i = 0; i < items.size(); i++) {
		vector<int> childs = items[i].childs;
		for (size_t j = 0; j < childs.size(); j++)
		{
			ss << i << '\t' << childs[j] << endl;
		}
	}

	return ss.str();
}

