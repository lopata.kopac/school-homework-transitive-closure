#include "pch.h"
#include "Relationship.h"
#include "Until.h"

using namespace std;

string getDefaultImputFile() { return "R100.txt"; }

string getDefaultoutputFile() { return "example.txt"; }

int main(int argc, char *argv[])
{
	string imputFilename = getDefaultImputFile();
	string outputFilename = getDefaultoutputFile();

	if (argc > 1)
		imputFilename = argv[1];
	if (argc > 2)
		outputFilename = argv[2];

	Relationship closure = readRelationshipFromFile(imputFilename);

	closure = closure.getTransitiveClosure();

	saveRelationship(outputFilename, closure);

	cout << "File " << imputFilename << " Succes converted into " << outputFilename << endl;

	return 0;
}
